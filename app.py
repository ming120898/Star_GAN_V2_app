import os
from flask import Flask, flash, request, redirect, url_for, render_template
import secrets
import cv2 as cv
import io
from base64 import b64encode
from PIL import Image
import random

#----------------------------Thư viện cho model----------------------------#
import argparse
import torch
from os.path import join as ospj
from core.model import build_model
from core.checkpoint import CheckpointIO
from PIL import Image
from torchvision import transforms
import torchvision.utils as vutils
from torchvision import transforms


#----------------------------Config---------------------------------------#
parser = argparse.ArgumentParser()
parser.add_argument('--img_size', type=int, default=256, help='Image resolution')
parser.add_argument('--num_domains', type=int, default=2, help='Number of domains')
parser.add_argument('--latent_dim', type=int, default=16, help='Latent vector dimension')
parser.add_argument('--style_dim', type=int, default=64, help='Style code dimension')
parser.add_argument('--w_hpf', type=float, default=1, help='weight for high-pass filtering')
parser.add_argument('--resume_iter', type=int, default=0, help='Iterations to resume training/testing')
parser.add_argument('--val_batch_size', type=int, default=32, help='Batch size for validation')
parser.add_argument('--checkpoint_dir', type=str, default='expr/checkpoints/celeba_hq', help='Directory for saving network checkpoints')
parser.add_argument('--wing_path', type=str, default='expr/checkpoints/wing.ckpt')
parser.add_argument('--num_workers', type=int, default=4, help='Number of workers used in DataLoader')
args, _ = parser.parse_known_args()


#----------------------------Biến khởi tạo app Flask----------------------------#
app = Flask(__name__)
secret = secrets.token_urlsafe(32)
app.secret_key = secret


#----------------------------Config app----------------------------#
#Thư mục upload ảnh khi người dùng tải lên
UPLOAD_FOLDER = 'static/uploads/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


#----------------------------Các hàm phụ trợ----------------------------#
def show_wait_destroy(win_name, image):
    """
    Function display image with auto ratio
    :param win_name: (string): name window
    :param image: (image) : image
    """
    cv.namedWindow(win_name, 0)
    cv.imshow(win_name, image)
    cv.waitKey(0)


def _load_checkpoint(step):
    for ckptio in ckptios:
        ckptio.load(step)


def save_image(x, ncol, filename):
    x = denormalize(x)
    vutils.save_image(x.cpu(), filename, nrow=ncol, padding=0)


def denormalize(x):
    out = (x + 1) / 2
    return out.clamp_(0, 1)


def translate_using_reference(nets, args, x_src, x_ref, y_ref):
    N, C, H, W = x_src.size()
    masks = nets.fan.get_heatmap(x_src) if args.w_hpf > 0 else None
    s_ref = nets.style_encoder(x_ref, y_ref)
    s_ref_list = s_ref.unsqueeze(1).repeat(1, N, 1)
    x_concat = []
    for i, s_ref in enumerate(s_ref_list):
        x_fake = nets.generator(x_src, s_ref, masks=masks)
        x_fake_with_ref = torch.cat([x_fake], dim=0)
        x_concat += [x_fake_with_ref]
    x_concat = torch.cat(x_concat, dim=0)
    #save_image(x_concat, N+1, filename)
    # del x_concat
    x_concat = denormalize(x_concat)
    return x_concat


@torch.no_grad()
def gen_img(image_src, image_ref, label):
    transform = transforms.Compose([
        transforms.Resize([256, 256]),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5],
                             std=[0.5, 0.5, 0.5]),
    ])
    image_src = transform(image_src)
    image_src.unsqueeze_(0)
    image_ref = transform(image_ref)
    image_ref.unsqueeze_(0)
    # 0: Nu, 1 : Nam
    label = torch.tensor(label)
    return translate_using_reference(nets_ema, args, image_src, image_ref, label)



#----------------------------Load model----------------------------#
nets, nets_ema = build_model(args)
ckptios = [CheckpointIO(ospj(args.checkpoint_dir, '100000_nets_ema.ckpt'), **nets_ema)]
_load_checkpoint(args.resume_iter)


#Load khi app khởi tạo
@app.route("/")
def upload_form():
    return render_template('upload.html')


#Xử lý khi file ảnh được upload lên
@app.route("/", methods=['POST'])
def upload_image():
    #Lấy loại khuôn mặt từ người dùng (nam hoặc nữ)
    type_face = request.form.get("type_face", "")
    print("Loại khuôn mặt : ", type_face)
    # Lấy loại cần sinh (ngẫu nhiên hoặc tùy chọn)
    type = request.form.get("type", "")
    print("Loại sinh ảnh : ", type)
    image_src = Image.open(request.files['fileupload'])
    # Lấy hình ảnh tải lên và chọn ngẫu nhiên 1 ảnh tham chiếu
    cwd = os.getcwd()
    temp = os.path.join("static","data_ref")
    path_dir_ori = os.path.join(cwd, temp)
    if int(type_face) == 1:
        path_dir_trg = os.path.join(path_dir_ori, "female")
        label = [0]
    else:
        path_dir_trg = os.path.join(path_dir_ori, "male")
        label = [1]
    if int(type) == 1:
        #Lấy ngẫu nhiên 1 đường dẫn ảnh
        path_rd = random.choice(os.listdir(os.path.expanduser(path_dir_trg)))
        print(os.path.join(path_dir_trg, path_rd))
        image_ref = Image.open(os.path.join(os.path.join(path_dir_trg, path_rd)))
    else:
        image_ref = Image.open(request.files['fileupload1'])
    #Image Gen
    image_predict = gen_img(image_src=image_src, image_ref=image_ref, label=label)[0]
    image_predict = transforms.ToPILImage()(image_predict)

    #Hiện thị lên web
    file_object = io.BytesIO()
    image_src.save(file_object, 'PNG')
    base64img = "data:image/png;base64," + b64encode(file_object.getvalue()).decode('ascii')
    # Trả về ảnh dự đoán
    file_object1 = io.BytesIO()
    image_predict.save(file_object1, 'PNG')
    image_predict = "data:image/png;base64," + b64encode(file_object1.getvalue()).decode('ascii')
    return render_template("upload.html", image=base64img, type=type_face, predict_img=image_predict)

if __name__ == "__main__":
    app.run()
